/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simulationepidemie.views;

import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.xy.DefaultXYDataset;

/**
 *
 * @author somir
 */
public final class ViewSIR extends ViewModele {
 
    /**
     *
     * @param S Tableau contenant le nombre de Personne saine au cours du temps
     * @param I Tableau contenatn le nombre de Personne infectée au cours du temps
     * @param R Tableau contenatn le nombre de Personne retirée au cours du temps
     * @param t Durée de la simulation (en jour)
     */
    public ViewSIR(int[] S, int[] I, int[] R, int t){
        super("Modele SIR");

        
        
        initializeChartPie(S,I,R,t);
        initializeChartTable(S,I,R,t);
        initializeChartXY(S,I,R,t);
        initializeActions();
    }
    
    /**
     *
     * @param S Tableau contenant le nombre de Personne saine au cours du temps
     * @param I Tableau contenatn le nombre de Personne infectée au cours du temps
     * @param R Tableau contenatn le nombre de Personne retirée au cours du temps
     * @param t Durée de la simulation (en jour)
     */
    public void initializeChartPie(int[] S, int[] I, int[] R, int t){
        DefaultPieDataset dataset = new DefaultPieDataset();
        dataset.setValue("SAINS", S[S.length - 2]);
        dataset.setValue("INFECTEES", I[I.length - 2]);
        dataset.setValue("RETIREES", R[R.length - 2]);
        
        JFreeChart chartPie = ChartFactory.createPieChart(
                                        "Modèle SIR",
                                        dataset,
                                        true, 
                                        true,
                                        false); 
        
        PiePlot plotPie = (PiePlot) chartPie.getPlot();
        plotPie.setSectionPaint("SAINS", Color.green);
        plotPie.setSectionPaint("INFECTEES", Color.red);
        plotPie.setSectionPaint("RETIREES", Color.blue);
        
        panelChartPie = new ChartPanel(chartPie);
        panelChartPie.setBounds(0,200, 583,500);  
        panelChartPie.setVisible(false);
        
        
        contentPanel.add(panelChartPie);
    }
    
    /**
     *
     * @param S Tableau contenant le nombre de Personne saine au cours du temps
     * @param I Tableau contenatn le nombre de Personne infectée au cours du temps
     * @param R Tableau contenatn le nombre de Personne retirée au cours du temps
     * @param t Durée de la simulation (en jour)
     */
    public void initializeChartTable(int[] S, int[] I, int[] R, int t){
        DefaultTableModel dtm = new DefaultTableModel(0, 0);

       // entete de la table
       String entete[] = new String[] { "Personnes saines", "Personnes infectées", "Personnes retirées"};

       // add header in table model     
        dtm.setColumnIdentifiers(entete);
           //set model into the table object
              tableau.setModel(dtm);

            // add row dynamically into the table      
       for (int count = 1; count <= t+1; count++) {
               dtm.addRow(new Object[] { S[count-1], I[count-1], R[count-1]});
        }
        
        contentPanel.add(tableau);
    }
    
    /**
     *
     * @param S Tableau contenant le nombre de Personne saine au cours du temps
     * @param I Tableau contenatn le nombre de Personne infectée au cours du temps
     * @param R Tableau contenatn le nombre de Personne retirée au cours du temps
     * @param t Durée de la simulation (en jour)
     */
    public void initializeChartXY(int[] S, int[] I, int[] R, int t){
        DefaultXYDataset datasetXY = new DefaultXYDataset();
        double[][] valuesS = new double[2][S.length];
        double[][] valuesI = new double[2][I.length];
        double[][] valuesR = new double[2][R.length];

        for(int i = 0; i<S.length; i++){
            valuesS[0][i] = i;
            valuesS[1][i] = S[i];

            valuesI[0][i] = i;
            valuesI[1][i] = I[i];

            valuesR[0][i] = i;
            valuesR[1][i] = R[i];
        }

        datasetXY.addSeries("S", valuesS);
        datasetXY.addSeries("I", valuesI);
        datasetXY.addSeries("R", valuesR);

        JFreeChart chartXY = ChartFactory.createTimeSeriesChart("Modèle SIR", "Jours", "Population", datasetXY);
        panelChartXY = new ChartPanel(chartXY);
        panelChartXY.setVisible(false);
        panelChartXY.setBounds(0,190, 583,500);    
        contentPanel.add(panelChartXY);
    }

}
