/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simulationepidemie.views;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTable;
import org.jfree.chart.ChartPanel;

/**
 *
 * @author somir
 */
public class ViewModele extends JFrame {
    JButton btn_pieChart;
    JButton btn_tableChart;
    JButton btn_chartXY;
    JButton retour;
    JTable tableau;
    ChartPanel panelChartPie;
    ChartPanel panelChartXY;
    JPanel contentPanel;
    
    /**
     *
     * @param title Titre de la fenêtre
     */
    public ViewModele(String title){
        super(title);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setSize(600, 800);
        contentPanel = (JPanel)this.getContentPane();
        
        contentPanel.setLayout(null);
        
        
        
        btn_pieChart = new JButton("Camembère");
        btn_pieChart.setBounds(35, 100, 150, 30);
        
        btn_tableChart = new JButton("Table");
        btn_tableChart.setBounds(210, 100, 150, 30);
        
        retour = new JButton("Retour");
        retour.setEnabled(false);
        retour.setBounds(450, 10, 100, 30);
        retour.setVisible(false);
        
        btn_chartXY = new JButton("Graphe XY");
        btn_chartXY.setBounds(390, 100, 150, 30);
        btn_chartXY.setVisible(true);
        
        contentPanel.add(btn_pieChart);
        contentPanel.add(btn_tableChart);
        contentPanel.add(retour);
        contentPanel.add(btn_chartXY);
        
        tableau = new JTable();
        tableau.setBounds(0,200, 583,500);
        tableau.setVisible(false);
    }
    
    /**
     * Initialise les actions des boutons de la fenêtre des modèles
     */
    public void initializeActions(){
        
        btn_pieChart.addActionListener(new ActionListener() {  
            public void actionPerformed(ActionEvent e){  
                retour.setEnabled(true);
                retour.setVisible(true);
                panelChartPie.setVisible(true);
             
                btn_tableChart.setEnabled(false);
                tableau.setVisible(false);
                
                btn_chartXY.setVisible(true);
                btn_chartXY.setEnabled(false);
                panelChartXY.setVisible(false);

            } 
        });

        btn_tableChart.addActionListener(new ActionListener() {  
            public void actionPerformed(ActionEvent e){  
                retour.setEnabled(true);
                retour.setVisible(true);
                btn_pieChart.setEnabled(false);
                panelChartPie.setVisible(false);
                
                btn_chartXY.setVisible(true);
                btn_chartXY.setEnabled(false);
                panelChartXY.setVisible(false);
                
                tableau.setVisible(true);
                btn_tableChart.setEnabled(true);
            } 
        });
        
        btn_chartXY.addActionListener(new ActionListener() {  
            public void actionPerformed(ActionEvent e){  
                retour.setEnabled(true);
                retour.setVisible(true);
                btn_pieChart.setEnabled(false);
                panelChartPie.setVisible(false);
                tableau.setVisible(false);
                btn_tableChart.setEnabled(false);
                
                panelChartXY.setVisible(true);
                btn_chartXY.setVisible(true);
            } 
        });
                
        retour.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                retour.setEnabled(false);
                btn_pieChart.setEnabled(true);
                btn_tableChart.setEnabled(true);
                btn_chartXY.setEnabled(true);

                panelChartPie.setVisible(false);
                tableau.setVisible(false);
                panelChartXY.setVisible(false);
            }
        });
    }
}
