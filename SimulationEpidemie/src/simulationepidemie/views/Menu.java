/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simulationepidemie.views;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import simulationepidemie.controllers.ControllerSEIR;
import simulationepidemie.controllers.ControllerSEIRnaiss;
import simulationepidemie.controllers.ControllerSIR;
import simulationepidemie.models.PolitiquePublique;
/**
 *
 * @author somir
 */ 
public final class Menu extends JFrame {
    
        JButton SIR;
        JButton SEIR;
        JButton SEIRnaiss;
        JButton simuler; 
        JButton retour;
        
        JTextField txtField_S;
        JTextField txtField_I;
        JTextField txtField_R;
        JTextField txtField_E;
        JTextField txtField_beta;
        JTextField txtField_gamma;
        JTextField txtField_alpha;
        JTextField txtField_mu;
        JTextField txtField_eta;
        JTextField txtField_dureeSimulation;
        JTextField txtField_tailleCarte;
        
        
        JLabel label_S;
        JLabel label_I;
        JLabel label_R;
        JLabel label_E;
        JLabel label_beta;
        JLabel label_gamma;
        JLabel label_alpha;
        JLabel label_mu;
        JLabel label_eta;
        JLabel label_dureeSimulation;
        JLabel label_tailleCarte;
        JLabel label_choixPolitquePublique;
        JLabel label_choixModele;

        JComboBox politiquePublique;
        
        Boolean SIR_isTrue;    
        Boolean SEIR_isTrue;
        Boolean SEIRnaiss_isTrue;
        
        JPanel contentPanel;

    /**
     *
     * @param width Largeur de la fenêtre
     * @param height hauteur de la fenêtre
     */
    public Menu(int width, int height){
        super("Menu");
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setSize(width, height);
        this.setLocationRelativeTo(this);
        
        contentPanel = (JPanel) this.getContentPane();
        contentPanel.setLayout(null);
        
            
        initializeTextFieldsAndLabels();
        initializeButtons();
        initializeChoixPolitiquePublique();
        initializeContentPanel();
        initializeActions();
    }
    
    /**
     * Initialise les éléments affichés dans la fenêtre
     */
    public void initializeTextFieldsAndLabels(){
        
        label_choixModele = new JLabel("Choisissez un modèle :"); 
        label_choixModele.setBounds(220, 100, 300, 30);
        
        label_S = new JLabel("Sains :"); 
        label_S.setBounds(70, 200, 70, 30);
        label_S.setVisible(false);
        
        txtField_S = new JTextField(5);
        txtField_S.setBounds(140 , 205, 200, 20);
        txtField_S.setVisible(false);
        
        label_I = new JLabel("Infectes :");
        label_I.setBounds(70, 230, 70, 30);
        label_I.setVisible(false);
        
        txtField_I = new JTextField(5);
        txtField_I.setBounds(140 , 235, 200, 20);
        txtField_I.setVisible(false);
        
        label_R = new JLabel("Retirees :");
        label_R.setBounds(70, 260, 70, 30);
        label_R.setVisible(false);
        txtField_R = new JTextField(5);
        txtField_R.setBounds(140 , 265, 200, 20);
        txtField_R.setVisible(false);        
        
        label_E = new JLabel("Exposees :");
        label_E.setBounds(70, 290, 70, 30);
        label_E.setVisible(false);        
        txtField_E = new JTextField(5);
        txtField_E.setBounds(140 , 295, 200, 20);
        txtField_E.setVisible(false);

        label_beta = new JLabel("Parametre beta :");
        label_beta.setBounds(20, 330, 200, 20);
        label_beta.setVisible(false);
        txtField_beta = new JTextField(5);
        txtField_beta.setBounds(140 , 330, 200, 20);
        txtField_beta.setVisible(false);
        
        label_gamma = new JLabel("Parametre gamma :");
        label_gamma.setBounds(20, 360, 200, 20);
        label_gamma.setVisible(false);
        txtField_gamma = new JTextField(5);
        txtField_gamma.setBounds(140 , 360, 200, 20);
        txtField_gamma.setVisible(false);
        
        label_alpha = new JLabel("Parametre alpha :");
        label_alpha.setBounds(20, 390, 200, 20);
        label_alpha.setVisible(false);
        txtField_alpha = new JTextField(5);
        txtField_alpha.setBounds(140 , 395, 200, 20);
        txtField_alpha.setVisible(false);
        
        label_eta = new JLabel("Parametre eta :");
        label_eta.setBounds(20, 420, 200, 20);
        label_eta.setVisible(false);
        txtField_eta = new JTextField(5);
        txtField_eta.setBounds(140 , 425, 200, 20);
        txtField_eta.setVisible(false);
        
        label_mu = new JLabel("Parametre mu :");
        label_mu.setBounds(20, 450, 200, 20);
        label_mu.setVisible(false);
        txtField_mu = new JTextField(5);
        txtField_mu.setBounds(140 , 455, 200, 20);
        txtField_mu.setVisible(false);
                
        label_dureeSimulation = new JLabel("Durée de la simulation :");
        label_dureeSimulation.setBounds(370, 205, 200, 50);
        label_dureeSimulation.setVisible(false);
        txtField_dureeSimulation = new JTextField(5);
        txtField_dureeSimulation.setBounds(370, 245, 130, 50);
        txtField_dureeSimulation.setVisible(false);
        
        label_tailleCarte = new JLabel("Taille de la carte :");
        label_tailleCarte.setBounds(370, 290, 200, 50);
        label_tailleCarte.setVisible(false);
        txtField_tailleCarte = new JTextField(5);
        txtField_tailleCarte.setBounds(370, 330, 100, 50);
        txtField_tailleCarte.setVisible(false);
        
        
        txtField_S.setText("100");
        txtField_I.setText("7");
        txtField_R.setText("1");
        txtField_E.setText("3");
        txtField_beta.setText("0.003");
        txtField_gamma.setText("0.02");
        txtField_alpha.setText("0.02");
        txtField_mu.setText("0.3");
        txtField_eta.setText("0.03");
        txtField_dureeSimulation.setText("10");
        txtField_tailleCarte.setText("2");
    }
    
    /**
     * Initialise les boutons de la fenêtre
     */
    public void initializeButtons(){
        SIR = new JButton("SIR");
        SIR.setBounds(80, 150, 100, 30);
        SIR_isTrue = false;
        
        SEIR = new JButton("SEIR");
        SEIR.setBounds(190, 150, 100, 30);
        SEIR_isTrue = false;
        
        SEIRnaiss = new JButton("SEIR avec naissance");
        SEIRnaiss.setBounds(300, 150, 200, 30);
        SEIRnaiss_isTrue = false;        
        

        
        simuler = new JButton("Simuler");
        simuler.setEnabled(false);
        simuler.setBounds(240, 600, 100, 60);
        simuler.setVisible(true);
           
        retour = new JButton("Retour");
        retour.setEnabled(false);
        retour.setBounds(450, 10, 100, 30);
        retour.setVisible(false);

    }
    
    /**
     * Initialise le menu déroulant du choix des politiques publiques
     */
    public void initializeChoixPolitiquePublique(){
        String [] listePolitiquePubliques = {PolitiquePublique.nomPolitiquePublique.AUCUNE.toString(), 
                                            PolitiquePublique.nomPolitiquePublique.CONFINEMENT.toString(),
                                            PolitiquePublique.nomPolitiquePublique.PORT_DU_MASQUE.toString(),
                                            PolitiquePublique.nomPolitiquePublique.QUARANTAINE.toString(),
                                            PolitiquePublique.nomPolitiquePublique.VACCINATION.toString()};
        politiquePublique = new JComboBox(listePolitiquePubliques);
        politiquePublique.setSelectedIndex(0);
        politiquePublique.setBounds(370, 440, 200, 30);
        politiquePublique.setVisible(false);
        
        label_choixPolitquePublique = new JLabel("Choisissez une politique publique :");
        label_choixPolitquePublique.setBounds(370, 400, 200, 50);
        label_choixPolitquePublique.setVisible(false);
    }
    
    /**
     * Initialise tous les éléments que l'on affiche dans le Content Panel
     */
    public void initializeContentPanel(){
        contentPanel.add("Center",label_choixModele);
        contentPanel.add("Center",SIR);
        contentPanel.add(SEIR);
        contentPanel.add(SEIRnaiss);
        contentPanel.add(simuler);
        contentPanel.add(txtField_S);
        contentPanel.add(txtField_I);
        contentPanel.add(txtField_R);
        contentPanel.add(txtField_E);
        contentPanel.add(label_S);
        contentPanel.add(label_I);
        contentPanel.add(label_R);
        contentPanel.add(label_E);
        contentPanel.add(txtField_beta);
        contentPanel.add(txtField_gamma);
        contentPanel.add(txtField_alpha);
        contentPanel.add(txtField_eta);
        contentPanel.add(txtField_mu);
        contentPanel.add(label_beta);
        contentPanel.add(label_gamma);
        contentPanel.add(label_alpha);
        contentPanel.add(label_eta);
        contentPanel.add(label_mu);
        contentPanel.add(retour);
        contentPanel.add(label_dureeSimulation);
        contentPanel.add(txtField_dureeSimulation);
        contentPanel.add(label_tailleCarte);
        contentPanel.add(txtField_tailleCarte);
        contentPanel.add(politiquePublique);
        contentPanel.add(label_choixPolitquePublique);
    }
    
    /**
     * Initialise les actions des boutons
     */
    public void initializeActions(){
        
        SIR.addActionListener(new ActionListener() {  
            public void actionPerformed(ActionEvent e){  
                SEIR.setEnabled(false);
                SEIRnaiss.setEnabled(false);
                
                txtField_S.setVisible(true);
                txtField_I.setVisible(true);
                txtField_R.setVisible(true);
                txtField_E.setVisible(false);
                txtField_beta.setVisible(true);
                txtField_gamma.setVisible(true);
                txtField_alpha.setVisible(false);
                label_S.setVisible(true);
                label_I.setVisible(true);
                label_R.setVisible(true);
                label_E.setVisible(false);
                label_beta.setVisible(true);    
                label_gamma.setVisible(true);   
                label_alpha.setVisible(false); 
                label_eta.setVisible(false);
                label_mu.setVisible(false);
                txtField_eta.setVisible(false);
                txtField_mu.setVisible(false);
                label_dureeSimulation.setVisible(true);
                txtField_dureeSimulation.setVisible(true);
                label_tailleCarte.setVisible(true);
                txtField_tailleCarte.setVisible(true);
                politiquePublique.setVisible(true);
                label_choixPolitquePublique.setVisible(true);
                retour.setVisible(true);
                retour.setEnabled(true);
                simuler.setEnabled(true);
                
                
                SIR_isTrue = true;  
            } 
        });
        
        SEIR.addActionListener(new ActionListener() {  
            public void actionPerformed(ActionEvent e){  
                SIR.setEnabled(false);
                SEIRnaiss.setEnabled(false);
                
                txtField_S.setVisible(true);
                txtField_I.setVisible(true);
                txtField_R.setVisible(true);
                txtField_E.setVisible(true);
                txtField_beta.setVisible(true);
                txtField_gamma.setVisible(true);
                txtField_alpha.setVisible(true);
                label_S.setVisible(true);
                label_I.setVisible(true);
                label_R.setVisible(true);
                label_E.setVisible(true);
                label_beta.setVisible(true);    
                label_gamma.setVisible(true);   
                label_alpha.setVisible(true); 
                label_eta.setVisible(false);
                label_mu.setVisible(false);
                txtField_eta.setVisible(false);
                txtField_mu.setVisible(false);
                label_dureeSimulation.setVisible(true);
                txtField_dureeSimulation.setVisible(true);
                label_tailleCarte.setVisible(true);
                txtField_tailleCarte.setVisible(true);
                politiquePublique.setVisible(true);
                label_choixPolitquePublique.setVisible(true);
                retour.setVisible(true);
                retour.setEnabled(true);
                simuler.setEnabled(true);
                
                SEIR_isTrue = true;
            } 
        });
        
        SEIRnaiss.addActionListener(new ActionListener() {  
            public void actionPerformed(ActionEvent e){  
                SIR.setEnabled(false);
                SEIR.setEnabled(false);
                
                txtField_S.setVisible(true);
                txtField_I.setVisible(true);
                txtField_R.setVisible(true);
                txtField_E.setVisible(true);
                txtField_beta.setVisible(true);
                txtField_gamma.setVisible(true);
                txtField_alpha.setVisible(true);
                label_S.setVisible(true);
                label_I.setVisible(true);
                label_R.setVisible(true);
                label_E.setVisible(true);
                label_beta.setVisible(true);    
                label_gamma.setVisible(true);   
                label_alpha.setVisible(true); 
                label_eta.setVisible(true);
                label_mu.setVisible(true);
                txtField_eta.setVisible(true);
                txtField_mu.setVisible(true);
                label_dureeSimulation.setVisible(true);
                txtField_dureeSimulation.setVisible(true);
                label_tailleCarte.setVisible(true);
                txtField_tailleCarte.setVisible(true);
                politiquePublique.setVisible(true);
                label_choixPolitquePublique.setVisible(true);                
                retour.setVisible(true);
                retour.setEnabled(true);       
                simuler.setEnabled(true);
                
                SEIRnaiss_isTrue = true;
             } 
        });
        
        retour.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                
                SIR.setEnabled(true);
                SEIR.setEnabled(true);
                SEIRnaiss.setEnabled(true);
                
                
                txtField_S.setVisible(false);
                txtField_I.setVisible(false);
                txtField_R.setVisible(false);
                txtField_E.setVisible(false);
                txtField_beta.setVisible(false);
                txtField_gamma.setVisible(false);
                txtField_alpha.setVisible(false);
                label_S.setVisible(false);
                label_I.setVisible(false);
                label_R.setVisible(false);
                label_E.setVisible(false);
                label_beta.setVisible(false);    
                label_gamma.setVisible(false);   
                label_alpha.setVisible(false); 
                label_eta.setVisible(false);
                label_mu.setVisible(false);
                txtField_eta.setVisible(false);
                txtField_mu.setVisible(false);
                label_dureeSimulation.setVisible(false);
                txtField_dureeSimulation.setVisible(false);
                label_tailleCarte.setVisible(false);
                txtField_tailleCarte.setVisible(false);
                retour.setEnabled(false);       
                simuler.setEnabled(false);                
                txtField_S.setText("100");
                txtField_I.setText("7");
                txtField_R.setText("1");
                txtField_E.setText("3");
                txtField_beta.setText("0.003");
                txtField_gamma.setText("0.02");
                txtField_alpha.setText("0.02");
                txtField_mu.setText("0.3");
                txtField_eta.setText("0.03");
                txtField_dureeSimulation.setText("10");
                txtField_tailleCarte.setText("2");
                SIR_isTrue = false;
                SEIR_isTrue = false;
                SEIRnaiss_isTrue = false;
                politiquePublique.setVisible(false);
                politiquePublique.setSelectedIndex(0);
                label_choixPolitquePublique.setVisible(false);  
                
            }
        });
        
        
        simuler.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                
                int S = Integer.parseInt(txtField_S.getText());
                int E = Integer.parseInt(txtField_E.getText());
                int I = Integer.parseInt(txtField_I.getText());
                int R = Integer.parseInt(txtField_R.getText());
                double beta = Double.parseDouble(txtField_beta.getText());
                double gamma = Double.parseDouble(txtField_gamma.getText());
                double alpha = Double.parseDouble(txtField_alpha.getText());
                double mu = Double.parseDouble(txtField_mu.getText());
                double eta = Double.parseDouble(txtField_eta.getText());
                
                int tailleCarte = Integer.parseInt(txtField_tailleCarte.getText());
                int dureeSimulation = Integer.parseInt(txtField_dureeSimulation.getText());
                
                if(SIR_isTrue){
                    double[] SIR = {S, I, R , beta, gamma};
                    ControllerSIR cSIR = new ControllerSIR(SIR, tailleCarte, dureeSimulation, (String)politiquePublique.getSelectedItem());
                }
                if(SEIR_isTrue){
                    double[] SEIR = {S, E, I, R, beta, gamma, alpha};
                    ControllerSEIR cSEIR = new ControllerSEIR(SEIR, tailleCarte, dureeSimulation, (String)politiquePublique.getSelectedItem());
                }
                if(SEIRnaiss_isTrue){
                    double[] SEIRnaiss = {S, E, I, R, beta, gamma, alpha, mu , eta};
                    ControllerSEIRnaiss cSEIRnaiss = new ControllerSEIRnaiss(SEIRnaiss, tailleCarte, dureeSimulation, (String)politiquePublique.getSelectedItem());
                }
            }
        });
        
        
    }
}
    