/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simulationepidemie.controllers;

import simulationepidemie.models.Carte;
import simulationepidemie.models.PolitiquePublique.nomPolitiquePublique;
import simulationepidemie.views.ViewSEIRnaiss;

/**
 *
 * @author somir
 */
public class ControllerSEIRnaiss{

    private int N;
    private int S;
    private int E;
    private int I;
    private int R; 
    private double beta;
    private double gamma;
    private double alpha;
    private double mu;
    private double eta;
    private Carte carte;
    private int t;
    
    private int[] tabS;
    private int[] tabE;
    private int[] tabI;
    private int[] tabR;
    
    /**
     *
     * @param SEIRnaiss Paramètres initiaux entrés par l'utilisateur
     * @param tailleCarte Taille de la carte
     * @param dureeSimulation Durée de la simulation (en jour)
     * @param politiquePublique Politique publique appliquée
     */
    public ControllerSEIRnaiss(double [] SEIRnaiss, int tailleCarte, int dureeSimulation, String politiquePublique){
        this.S = (int)SEIRnaiss[0];
        this.E = (int)SEIRnaiss[1];
        this.I = (int)SEIRnaiss[2];
        this.R = (int)SEIRnaiss[3];
        this.N = this.S + this.E + this.I + this.R;
        this.beta = SEIRnaiss[4];
        this.gamma = SEIRnaiss[5];
        this.alpha = SEIRnaiss[6];
        this.mu = SEIRnaiss[7];
        this.eta = SEIRnaiss[8];
        switch(politiquePublique){
            case "CONFINEMENT":
                this.carte = new Carte(tailleCarte, S, E, I, R, nomPolitiquePublique.CONFINEMENT );
                break;
            case "PORT_DU_MASQUE":
                this.carte = new Carte(tailleCarte, S, E, I, R, nomPolitiquePublique.PORT_DU_MASQUE );
                break;
            case "QUARANTAINE":
                this.carte = new Carte(tailleCarte, S, E, I, R, nomPolitiquePublique.QUARANTAINE );
                break;
            case "VACCINATION":
                this.carte = new Carte(tailleCarte, S, E, I, R, nomPolitiquePublique.VACCINATION );
                break;  
            default:
                this.carte = new Carte(tailleCarte, S, E, I, R, nomPolitiquePublique.AUCUNE );
                break;
        }
        this.t = dureeSimulation;
        
        this.tabS = new int[tailleCarte + 1];
        this.tabE = new int[tailleCarte + 1];
        this.tabI = new int[tailleCarte + 1];
        this.tabR = new int[tailleCarte + 1];
        
        show();
    }
    
    /**
     * Lance la simulation du modèle SEIR avec variation de la population
     */
    public void simulerSEIRnaiss(){
        this.carte.simulerModeleSEIRnaiss(this.t, this.beta, this.gamma, this.alpha, this.mu, this.eta);
        this.S = this.carte.getNbSaines();
        this.E = this.carte.getNbExposees();
        this.I = this.carte.getNbInfectees();
        this.R = this.carte.getNbRetirees();

        this.tabS = this.carte.getS();
        this.tabE = this.carte.getE();
        this.tabI = this.carte.getI();
        this.tabR = this.carte.getR();
    }
    
    /**
     * Affiche la fenêtre de résultats de la simulation du modèle SEIR avec
     * variation de la population
     */
    public void show() {
        simulerSEIRnaiss();
        ViewSEIRnaiss viewSEIRnaiss = new ViewSEIRnaiss(this.tabS, this.tabE, this.tabI, this.tabR, t);  
        viewSEIRnaiss.setVisible(true);
    }
    
}
