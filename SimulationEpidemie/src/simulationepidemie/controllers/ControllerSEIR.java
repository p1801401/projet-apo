/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simulationepidemie.controllers;

import simulationepidemie.models.Carte;
import simulationepidemie.models.PolitiquePublique.nomPolitiquePublique;
import simulationepidemie.views.ViewSEIR;

/**
 *
 * @author somir
 */
public class ControllerSEIR{
    private int N;
    private int S;
    private int E;
    private int I;
    private int R; 
    private double beta;
    private double gamma;
    private double alpha;
    private Carte carte;
    private int t;
    
    private int[] tabS;
    private int[] tabE;
    private int[] tabI;
    private int[] tabR;
    
    /**
     *
     * @param SEIR Paramètres initiaux entrés par l'utilisateur
     * @param tailleCarte Taille de la carte
     * @param dureeSimulation Durée de la simulation (en jour)
     * @param politiquePublique Politique publique appliquée
     */
    public ControllerSEIR(double [] SEIR, int tailleCarte, int dureeSimulation, String politiquePublique){
        this.S = (int)SEIR[0];
        this.E = (int)SEIR[1];
        this.I = (int)SEIR[2];
        this.R = (int)SEIR[3];
        this.N = this.N = this.S + this.E + this.I + this.R;
        this.beta = SEIR[4];
        this.gamma = SEIR[5];
        this.alpha = SEIR[6];
        
        switch(politiquePublique){
            case "CONFINEMENT":
                this.carte = new Carte(tailleCarte, S, E, I, R, nomPolitiquePublique.CONFINEMENT );
                break;
            case "PORT_DU_MASQUE":
                this.carte = new Carte(tailleCarte, S, E, I, R, nomPolitiquePublique.PORT_DU_MASQUE );
                break;
            case "QUARANTAINE":
                this.carte = new Carte(tailleCarte, S, E, I, R, nomPolitiquePublique.QUARANTAINE );
                break;
            case "VACCINATION":
                this.carte = new Carte(tailleCarte, S, E, I, R, nomPolitiquePublique.VACCINATION );
                break;  
            default:
                this.carte = new Carte(tailleCarte, S, E, I, R, nomPolitiquePublique.AUCUNE );
                break;
        }
        
        this.t = dureeSimulation;
        
        this.tabS = new int[tailleCarte + 1];
        this.tabE = new int[tailleCarte + 1];
        this.tabI = new int[tailleCarte + 1];
        this.tabR = new int[tailleCarte + 1];
        show();
    }

    /**
     *
     * @return
     */
    public int getN() {
        return N;
    }

    /**
     *
     * @return
     */
    public int getS() {
        return S;
    }

    /**
     *
     * @return
     */
    public int getE() {
        return E;
    }

    /**
     *
     * @return
     */
    public int getI() {
        return I;
    }

    /**
     *
     * @return
     */
    public int getR() {
        return R;
    }

    /**
     *
     * @return
     */
    public double getBeta() {
        return beta;
    }

    /**
     *
     * @return
     */
    public double getGamma() {
        return gamma;
    }

    /**
     *
     * @return
     */
    public double getAlpha() {
        return alpha;
    }

    /**
     *
     * @return
     */
    public Carte getCarte() {
        return carte;
    }

    /**
     *
     * @return
     */
    public int getT() {
        return t;
    }
    
    /**
     * Lance la simulation du modèle SEIR
     */
    public void simulerSEIR(){
        this.carte.simulerModeleSEIR(this.t, this.beta, this.gamma, this.alpha);
        this.S = this.carte.getNbSaines();
        this.E = this.carte.getNbExposees();
        this.I = this.carte.getNbInfectees();
        this.R = this.carte.getNbRetirees();
        
        this.tabS = this.carte.getS();
        this.tabE = this.carte.getE();
        this.tabI = this.carte.getI();
        this.tabR = this.carte.getR();
    }

    /**
     * Affiche la fenêtre de résultats de la simulation du modèle SEIR
     */
    public void show() {
        simulerSEIR();
        ViewSEIR viewSEIR = new ViewSEIR(this.tabS, this.tabE, this.tabI, this.tabR, t);  
        viewSEIR.setVisible(true);
    }
}
