/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simulationepidemie.controllers;

import simulationepidemie.models.Carte;
import simulationepidemie.models.PolitiquePublique.nomPolitiquePublique;
import simulationepidemie.views.ViewSIR;

/**
 *
 * @author somir
 */
public class ControllerSIR{
    private int N;
    private int S;
    private int I;
    private int R; 
    private double beta;
    private double gamma;
    private Carte carte;
    private int t;
    
    private int[] tabS;
    private int[] tabI;
    private int[] tabR;
    
    /**
     *
     * @param SIR Paramètres initiaux entrés par l'utilisateur
     * @param tailleCarte Taille de la carte
     * @param dureeSimulation Durée de la simulation (en jour)
     * @param politiquePublique Politique publique appliquée
     */
    public ControllerSIR(double[] SIR, int tailleCarte, int dureeSimulation, String politiquePublique){
        
        this.S = (int)SIR[0];
        this.I = (int)SIR[1];
        this.R = (int)SIR[2];
        this.N = this.S + this.I + this.R;
        this.beta = SIR[3];
        this.gamma = SIR[4];
        
        switch(politiquePublique){
            case "CONFINEMENT":
                this.carte = new Carte(tailleCarte, S, I, R, nomPolitiquePublique.CONFINEMENT );
                break;
            case "PORT_DU_MASQUE":
                this.carte = new Carte(tailleCarte, S, I, R, nomPolitiquePublique.PORT_DU_MASQUE );
                break;
            case "QUARANTAINE":
                this.carte = new Carte(tailleCarte, S, I, R, nomPolitiquePublique.QUARANTAINE );
                break;
            case "VACCINATION":
                this.carte = new Carte(tailleCarte, S, I, R, nomPolitiquePublique.VACCINATION );
                break;  
            default:
                this.carte = new Carte(tailleCarte, S, I, R, nomPolitiquePublique.AUCUNE );
                break;
        }
        
        this.tabS = new int[tailleCarte + 1];
        this.tabI = new int[tailleCarte + 1];
        this.tabR = new int[tailleCarte + 1];
        
        this.t = dureeSimulation;
        show();
    }
    
    /**
     * Lance la simulation du modèle SIR
     */
    public void simulerSIR(){
        this.carte.simulerModeleSIR(this.t, this.beta, this.gamma);
        this.S = this.carte.getNbSaines();
        this.I = this.carte.getNbInfectees();
        this.R = this.carte.getNbRetirees();
        
        this.tabS = this.carte.getS();
        this.tabI = this.carte.getI();
        this.tabR = this.carte.getR();
    }
    
    /**
     * Affiche la fenêtre de résultats de la simulation du modèle SIR
     */
    public void show(){
        simulerSIR();        
        ViewSIR viewSIR = new ViewSIR(this.tabS, this.tabI, this.tabR, t);  
        viewSIR.setVisible(true);
    }
}
