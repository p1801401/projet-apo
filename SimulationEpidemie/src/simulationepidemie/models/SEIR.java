/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simulationepidemie.models;

/*
 *
 * @author somir
 */

public class SEIR extends Modele{
    
    /**
     * Nombre de Personne exposées dans le modèle
     */
    protected int E;

    /**
     * Probabilté qu'une personne exposée devienne infectée dans le modèle
     */
    protected double alpha;
    
    /**
     *
     * @param S Nombre de Personne saines
     * @param E Nombre de Personne exposées
     * @param I Nombre de Personne infectées
     * @param R Nombre de Personne retirées
     * @param beta Probabilité qu'une personne infectée expose une personne saine
     * @param gamma Probabilité de ne plus être infecté
     * @param alpha Probabilté qu'une personne exposée devienne infectée
     */
    public SEIR(int S, int E, int I, int R, double beta, double gamma, double alpha){
        super(S, I, R, beta, gamma);
        this.E = E;
        this.N = this.N + this.E;
        this.alpha = alpha;
    }
    
    /**
     *
     * @return Nombre de Personnes exposées
     */
    public int getNbExposees(){
        return this.E;
    }
    
    /**
     *
     * @return La valeur de alpha
     */
    public double getAlpha(){
        return this.alpha;
    }
    
    /**
     * Execute le modèle SEIR
     * @param t Durée de la simulation (en jour)
     */
    @Override
    public void executer(double t){
        double dS = this.S;
        double dI = this.I;
        double dR = this.R;
        double dE = this.E;
        
        int buffS;
        int buffE;
        int buffI;
        int buffR;
        double dS_precedent = this.S;
        double dE_precedent = this.E;
        double dI_precedent = this.I;
        double dR_precedent = this.R;
        
        while(t>0){
            
            dS_precedent = dS;
            dI_precedent = dI;
            dR_precedent = dR;
            dE_precedent = dE;
            dS = dS_precedent - ( this.beta*dS_precedent*dI_precedent);
            dE = dE_precedent + (this.beta*dS_precedent*dI_precedent - this.alpha*dE_precedent);
            dI = dI_precedent + (this.alpha*dE_precedent - this.gamma*dI_precedent);
            dR = dR_precedent + (this.gamma*dI_precedent);
            
            if(dS <= 0 || dE <= 0 || dI <= 0 || dR <= 0){
                t = 0;
            }
                        
            t--;
        }
        

        if(dS - (int)dS >= (double)0.5){
            buffS = (int)dS + 1;
        } else{
            buffS = (int)dS;
        }
        
        if(dE - (int)dE >= (double)0.5){
            buffE = (int)dE + 1;
        } else{
            buffE = (int)dE;
        }
        
        if(dI - (int)dI >= (double)0.5){
            buffI = (int)dI + 1;
        } else{
            buffI = (int)dI;
        }
        
        if(dR - (int)dR >= (double)0.5){
            buffR = (int)dR + 1;
        } else{
            buffR = (int)dR;
        }
        
        this.S = buffS;
        this.E = buffE;
        this.I = buffI;
        this.R = buffR;
    }
}
