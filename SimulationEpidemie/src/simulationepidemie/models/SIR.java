/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simulationepidemie.models;
import java.util.concurrent.TimeUnit;
/*
 *
 * @author somir
 */

public class SIR extends Modele {
    
    /**
     *
     * @param S Nombre de Personnes saines
     * @param I Nombre de Personnes infectées
     * @param R Nombre de Personnes retirées
     * @param beta Probabilité qu'une personne infectée contamine une personne saine
     * @param gamma Probabilité de ne plus être infecté
     */
    public SIR(int S, int I, int R, double beta, double gamma){
        super(S, I, R, beta, gamma);   
    }
    
    /**
     * Constructeur par défaut
     */
    public SIR(){
        super(0,0,0,0,0);
    }
    
    
    /**
     * Execute le modèle SIR
     * @param t Durée de la simulation (en jour)
     */
    @Override
    public void executer(double t){
        double dS = this.S;
        double dI = this.I;
        double dR = this.R;
        double dS_precedent = this.S;
        double dI_precedent = this.I;
        double dR_precedent = this.R;
        
        while(t>0){
            
            
            dS_precedent = dS;
            dI_precedent = dI;
            dR_precedent = dR;
            dS = dS_precedent - ( this.beta*dS_precedent*dI_precedent);
            dI = dI_precedent + (this.beta*dS_precedent*dI_precedent - this.gamma*dI_precedent);
            dR = dR_precedent + (this.gamma*dI_precedent);
            if(dS <= 0 || dI <= 0 || dR <= 0){
                t = 0;
            }
            t--;
        }
        
        if(dS - (int)dS >= 0.5){
            this.S = (int)dS + 1;
        } else{
            this.S = (int)dS;
        }
        
        if(dI - (int)dI >= 0.5){
            this.I = (int)dI + 1;
        } else{
            this.I = (int)dI;
        }
        
        if(dR - (int)dR >= 0.5){
            this.R = (int)dR + 1;
        } else{
            this.R = (int)dR;
        }
    }
}
