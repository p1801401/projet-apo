/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simulationepidemie.models;


/*
 *
 * @author somir
 */

public abstract class Modele {
    
    /**
     * Nombre total de Personne dans le modèle
     */
    protected int N;

    /**
     * Nombre de Personnes saines dans le modèle
     */
    protected int S;

    /**
     * Nombre de Personnes infectées dans le modèle
     */
    protected int I;

    /**
     * Nombre de Personnes retirées dans le modèle
     */
    protected int R; 

    /**
     * Probabilité qu'une personne infectée contamine une personne saine
     */
    protected double beta;

    /**
     * Probabilité de ne plus être infecté
     */
    protected double gamma;
    
    /**
     * Méthode abstraite qui exécute le modèle
     * @param t Durée de la simulation (en jour)
     */
    public abstract void executer(double t);
    
    /**
     * Constructeur par copie
     * @param modele Un modèle à copier
     */
    public Modele(Modele modele){
        this.S = modele.getNbSaines();
        this.I = modele.getNbInfectees();
        this.R = modele.getNbRetirees();
        this.N = this.S + this.I + this.R;
        this.beta = modele.getBeta();
        this.gamma = modele.getGamma();
    }
    
    /**
     *
     * @param S Nombre de Personnes saines
     * @param I Nombre de Personnes infectées
     * @param R Nombre de Personnes retirées
     * @param beta Probabilité qu'une personne infectée contamine une personne saine
     * @param gamma Probabilité de ne plus être infecté
     */
    public Modele(int S, int I, int R, double beta, double gamma){
        this.S = S;
        this.I = I;
        this.R = R;
        this.N = this.S + this.I + this.R;
        this.beta = beta;
        this.gamma = gamma;
    }
    
    /**
     *
     * @return Nombre de Personnes saines dans le modèle
     */
    public int getNbSaines() {

        return this.S;
    }

    /**
     *
     * @return Nombre de Personnes infectées dans le modèle
     */
    public int getNbInfectees(){
        return this.I;
    }
    
    /**
     *
     * @return Nombre de Personnes retirées dans le modèle
     */
    public int getNbRetirees(){
        return this.R;        
    }

    /**
     *
     * @return Probabilité qu'une personne infectée contamine une personne saine dans le modèle
     */
    public double getBeta(){        
        return this.beta;
    }

    /**
     *
     * @return Probabilité de ne plus être infecté dans le modèle
     */
    public double getGamma(){        
        return this.gamma;
    }

    /**
     *
     * @param beta Probabilité qu'une personne infectée contamine 
     * une personne saine que l'on souhaite mettre dans le modèle
     */


    public void setBeta(double beta) {
        this.beta = beta;
    }

}
