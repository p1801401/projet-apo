/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simulationepidemie.models;

/**
 *
 * @author somir
 */
public class SEIRnaiss extends SEIR {

    /**
     * Proportion de Personnes qui meurent naturellement dans le modèle
     */
    protected double mu;

    /**
     * Proportion de Personnes qui naissent dans le modèle
     */
    protected double eta;
    
    /**
     *
     * @param S Nombre de Personnes saines
     * @param I Nombre de Personnes infectées
     * @param R Nombre de Personnes retirées
     * @param E Nombre de Personnes exposées
     * @param beta Probabilité qu'une personne infectée contamine une personne saine
     * @param gamma Probabilité de ne plus être infecté
     * @param alpha Probabilté qu'une personne exposée devienne infectée
     * @param mu Proportion de Personnes qui meurent naturellement
     * @param eta Proportion de Personnes qui naissent
     */
    public SEIRnaiss(int S, int I, int R, int E, double beta, double gamma, double alpha, double mu, double eta){
        super(S, I, R, E, beta, gamma, alpha);
        this.mu = mu;
        this.eta = eta;
        this.N = this.S + this.I + this.R + this.E;    
    }
    
    /**
     * Execute le modèle SEIR avec variation dans la population
     * @param t Durée de la simulation (en jour)
     */
    @Override
    public void executer(double t){
        double dS = this.S;
        double dI = this.I;
        double dR = this.R;
        double dE = this.E;
        double dN = this.N;
        double dS_precedent = this.S;
        double dE_precedent = this.E;
        double dI_precedent = this.I;
        double dR_precedent = this.R;
        double dN_precedent = this.N;
        
        while(t>0){
            
                        
            dS_precedent = dS;
            dI_precedent = dI;
            dR_precedent = dR;
            dE_precedent = dE;
            dN_precedent = dN;
            
            dS = dS_precedent + (- this.beta*dS_precedent*dI_precedent + this.eta*dN_precedent - this.mu*dS_precedent);
            dE = dE_precedent + (this.beta*dS_precedent*dI_precedent - this.alpha*dE_precedent - this.mu*dE_precedent);
            dI = dI_precedent + (this.beta*dS_precedent*dI_precedent - this.gamma*dI_precedent - this.mu*dI_precedent);
            dR = dR_precedent + (this.gamma*dI_precedent - this.mu*dR_precedent);
            dN = dS + dI + dR + dE;
            
            if(dS <= 0 || dE <= 0 || dI <= 0 || dR <= 0){
                t = 0;
            }
            t--;
        }
        
        if(dS - (int)dS >= 0.5){
            this.S = (int)dS + 1;
        } else{
            this.S = (int)dS;
        }
        
        if(dE - (int)dE >= 0.5){
            this.E = (int)dE + 1;
        } else{
            this.E = (int)dE;
        }
        
        if(dI - (int)dI >= 0.5){
            this.I = (int)dI + 1;
        } else{
            this.I = (int)dI;
        }
        
        if(dR - (int)dR >= 0.5){
            this.R = (int)dR + 1;
        } else{
            this.R = (int)dR;
        }
    }
}
