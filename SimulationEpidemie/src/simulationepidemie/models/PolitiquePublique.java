/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simulationepidemie.models;
import java.util.Random;
import simulationepidemie.models.Personne.Categorie;

/*
 *
 * @author somir
 */

public class PolitiquePublique {
    
    /**
     * Les différentes politiques publiques possibles
     */
    public enum nomPolitiquePublique { 

        /**
         * Aucune politique publique
         */
        AUCUNE, 

        /**
         * Politique publique du confinement
         */
        CONFINEMENT, 

        /**
         * Politique publique du port du masque
         */
        PORT_DU_MASQUE, 

        /**
         * Politique publique de la quarantaine
         */
        QUARANTAINE, 

        /**
         * Politique publique de la vaccination
         */
        VACCINATION};
    
     /**
     * Nom de la Politique publique
     */
    private nomPolitiquePublique nom;
    
    /**
     * Carte sur laquelle la politique publique est appliquée
     */
    private Carte carte;
    
    /**
     * Modèle sur lequel la politique publique est appliquée
     */
    private Modele modele; 
    
    /**
     *
     * @param nom Nom de la politique publique
     * @param carte Carte sur laquelle la poiltique publique est appliquée
     */
    public PolitiquePublique(nomPolitiquePublique nom, Carte carte){
        this.nom = nom;
        this.carte = new Carte(carte);
    }

    /**
     *
     * @return Le nom de la politique publique
     */
    public nomPolitiquePublique getNom() {
        return nom;
    }

    /**
     *
     * @return La carte liée à la politique publique
     */
    public Carte getCarte() {
        return carte;
    }

    /**
     *
     * @return Le modèle lié à la politique publique
     */
    public Modele getModele() {
        return modele;
    }

    /**
     *
     * @param nom Nouvelle politique publique qui remplace celle actuelle
     */
    public void setNom(nomPolitiquePublique nom) {
        this.nom = nom;
    }

    /**
     *
     * @param modele Nouveau modèle sur lequel la politique
     * publique va s'appliquer
     */
    public void setModele(Modele modele) {
        this.modele = modele;
    }
    
    /**
     * Applique la politique publique sur le modèle avec la carte associée
     */
    public void appliquer(){
        Personne [] tabPersonnes = carte.getTabPersonnes();
        switch(nom){
            case CONFINEMENT:       
                for(int i = 0; i < tabPersonnes.length; i++) {
                            tabPersonnes[i].setDeplacement(false);
                    }
                this.carte.setTabPersonnes(tabPersonnes);
                this.carte.updateCarte();
                break;
            case PORT_DU_MASQUE:
                double oldBeta = modele.getBeta();
                modele.setBeta(oldBeta/2);
                break;                
            case QUARANTAINE:               
                for(int i=0; i<tabPersonnes.length; i++){
                    if(tabPersonnes[i].getCategorie() == Categorie.INFECTEE){
                        tabPersonnes[i].setDeplacement(false);
                    }
                }
                this.carte.setTabPersonnes(tabPersonnes);
                this.carte.updateCarte();              
                break;
            case VACCINATION:
                int nbPersonnesSaines = 0;
                int tabIndice [] = new int [tabPersonnes.length];
                for(int i = 0; i < tabPersonnes.length; i++) {
                    if(tabPersonnes[i].getCategorie() == Categorie.SAINE) {
                        tabIndice[nbPersonnesSaines] = i;
                        nbPersonnesSaines++;
                    }
                }
                if(nbPersonnesSaines > 0) {
                    int nbVaccine = (nbPersonnesSaines / 10) + 1; // valeur entre 0 et 10% des sains
                    Random random = new Random();
                    int randPersonne;
                    int maxPositionXY = carte.getTailleCarte(); 
                    int randX, randY; 
                    
                    this.carte.setNbSaines(this.carte.getNbSaines() - nbVaccine);
                    this.carte.setNbRetirees(this.carte.getNbRetirees() + nbVaccine);
                    
                    while(nbVaccine > 0){
                        randPersonne = random.nextInt(nbPersonnesSaines); // choisi une personne saine
                        tabPersonnes[tabIndice[randPersonne]].setCategorie(Categorie.RETIREE);
                        nbVaccine--;
                    }
                }
                
                this.carte.setTabPersonnes(tabPersonnes);
                this.carte.updateCarte();         
                break;
            default: 
                break;
        }
                    
    }
}
