/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simulationepidemie.models;

import static java.lang.Math.abs;
import java.util.Random;
import java.util.ArrayList;
import simulationepidemie.models.Personne.Categorie;
import simulationepidemie.models.PolitiquePublique.nomPolitiquePublique;
/**
 *
 * @author adrbe
 */
public class Carte{
    
    /**
    * Tableau 2D représentant la carte et contenant des ArrayList de Personnes
    */
    private ArrayList<Personne> [][] carte;
    
    /**
    * Taille d'un côté de la carte
    */
    private int tailleCarte;
    
    /**
    * Tableau contenant toutes les Personnes présentes sur la carte
    */
    private Personne [] tabPersonnes;
    
    /**
    * Nombre de Personnes sur la carte
    */
    private int nbPersonnes;
    
    /**
    * Nombre de Personnes saines sur la carte
    */
    private int nbSaines;
    
    /**
    * Nombre de Personnes exposées sur la carte
    */
    private int nbExposees;
    
    /**
    * Nombre de Personnes infectées sur la carte
    */
    private int nbInfectees;
    
    /**
    * Nombre de Personnes retirées sur la carte
    */
    private int nbRetirees;
    
    /**
    * Politique Publique associée à la carte
    */
    private PolitiquePublique politiquePublique;
    
    /**
    * Tableau contenant toutes les Personnes saines présentes sur la carte
    */
    private int[] S;
    
    /**
    * Tableau contenant toutes les Personnes exposées présentes sur la carte
    */
    private int[] E;
    
    /**
    * Tableau contenant toutes les Personnes infectées présentes sur la carte
    */
    private int[] I;
    
    /**
    * Tableau contenant toutes les Personnes retirées présentes sur la carte
    */
    private int[] R;
    
    //modèle SEIR

    /**
     *
     * @param taille taille des côtés de la carte
     * @param nbS nombre de personnes saines
     * @param nbE nombre de personnes exposées
     * @param nbI nombre de personnes infectées
     * @param nbR nombre de personnes retirées
     * @param nom Politique publique appliquée
     */
    public Carte(int taille, int nbS, int nbE, int nbI, int nbR, nomPolitiquePublique nom) {
        //List l = new ArrayList<Personne>();
        this.carte = new ArrayList [taille][taille];
        for (int i = 0; i < taille; i++) {
            for (int j = 0; j < taille; j++) {
                this.carte[i][j] = new ArrayList<>();
            }
        }
        this.tailleCarte = taille;
        this.nbSaines = nbS;
        this.nbExposees = nbE;
        this.nbInfectees = nbI;
        this.nbRetirees = nbR;
        this.nbPersonnes = nbS + nbE + nbI + nbR;
        this.tabPersonnes = new Personne [this.nbPersonnes];
        this.politiquePublique = new PolitiquePublique(nom,this);

        repartir();
        
    }
    
    //modèle SIR

    /**
     *
     * @param taille taille des côtés de la carte
     * @param nbS nombre de personnes saines
     * @param nbI nombre de personnes infectées
     * @param nbR nombre de personnes retirées
     * @param nom nom de la politique publique appliquée
     */
    public Carte(int taille, int nbS, int nbI, int nbR, nomPolitiquePublique nom) {
        this(taille,nbS,0,nbI,nbR,nom);
        
    }
    
    /**
     *
     * @param c carte à copier
     */
    public Carte(Carte c) {
        this.carte = c.carte;
        this.tailleCarte = c.tailleCarte;
        this.tabPersonnes = c.tabPersonnes;
        this.nbPersonnes = c.nbPersonnes;
        this.nbSaines = c.nbSaines;
        this.nbExposees = c.nbExposees;
        this.nbInfectees = c.nbInfectees;
        this.nbRetirees = c.nbRetirees;
    }
    
    /**
     * Permet de cloner/copier une carte
     * @param c carte à copier
     */
    public void clone(Carte c) {
        this.carte = c.carte;
        this.tailleCarte = c.tailleCarte;
        this.tabPersonnes = c.tabPersonnes;
        this.nbPersonnes = c.nbPersonnes;
        this.nbSaines = c.nbSaines;
        this.nbExposees = c.nbExposees;
        this.nbInfectees = c.nbInfectees;
        this.nbRetirees = c.nbRetirees;
    }
    
   
    /**
     *
     * @return nombre de personnes saines dans la carte
     */
    public int getNbSaines() {
        return nbSaines;
    }
    
    /**
     *
     * @return nombre de personnes exposées dans la carte
     */
    public int getNbExposees() {
        return nbExposees;
    } 
    
    /**
     *
     * @return nombre de personnes infectées dans la carte
     */
    public int getNbInfectees() {
        return nbInfectees;
    } 
    
    /**
     *
     * @return nombre de personnes retirées dans la carte
     */
    public int getNbRetirees() {
        return nbRetirees;
    }
    
    /**
     *
     * @return la taille de la carte
     */
    public int getTailleCarte() {
        return this.tailleCarte;
    }
    
    /**
     *
     * @return le nombre de personnes total
     */
    public int getNbPersonnes() {
        return this.nbPersonnes;
    }

    /**
     *
     * @return la politique publique appliquée
     */
    public PolitiquePublique getPolitiquePublique() {
        return politiquePublique;
    }
    
    /**
     *
     * @return le tableau contenant toutes les personnes présentes dans la carte
     */
    public Personne [] getTabPersonnes() {
        return this.tabPersonnes;
    }

    /**
     *
     * @param tabPersonnes tableau de personnes 
     */
    public void setTabPersonnes(Personne[] tabPersonnes) {
        this.nbPersonnes = tabPersonnes.length;
        this.tabPersonnes = tabPersonnes;
    }
    
    /**
     *
     * @param nbS nombre de personnes saines 
     */
    public void setNbSaines(int nbS) {
        this.nbSaines = nbS;
    }
    
    /**
     *
     * @param nbE nombre de personnes exposées
     */
    public void setNbExposees(int nbE) {
        this.nbExposees = nbE;
    } 
    
    /**
     *
     * @param nbI nombre de personnes infectées
     */
    public void setNbInfectees(int nbI) {
        this.nbInfectees = nbI;
    } 
    
    /**
     *
     * @param nbR nombre de personnes retirées
     */
    public void setNbRetirees(int nbR) {
        this.nbRetirees = nbR;
    }

    /**
     *
     * @param nom Politique publique à appliquer
     */
    public void setPolitiquePublique(nomPolitiquePublique nom) {
        this.politiquePublique.setNom(nom);
    }
    
    /**
     * Applique la politique publique
     */
    public void appliquerPolPubl() {
        this.politiquePublique.appliquer();
    }

    /**
     *
     * @return un tableau des personnes saines présentes sur la carte
     */
    public int[] getS() {
        return S;
    }

    /**
     *
     * @return un tableau des personnes exposées présentes sur la carte
     */
    public int[] getE() {
        return E;
    }

    /**
     *
     * @return un tableau des personnes infectées présentes sur la carte
     */
    public int[] getI() {
        return I;
    }

    /**
     *
     * @return un tableau des personnes retirées présentes sur la carte
     */
    public int[] getR() {
        return R;
    }
    
    
    
    

    /**
     * Réparti sur la carte un nombre nbPersonnes de Personne de manière aléatoire.
     */
    public void repartir() {
        Random rand = new Random();
        int nbMax = this.tailleCarte;
        //int val [] = new int [2];
        int pos_x,pos_y;
        int indiceDebut;
        int indiceFin = nbSaines;
        //repartition personnes saines
        for (indiceDebut = 0; indiceDebut < indiceFin; indiceDebut++) {
            pos_x = rand.nextInt(nbMax);
            pos_y = rand.nextInt(nbMax);
            Personne p = new Personne(indiceDebut,Categorie.SAINE,pos_x,pos_y,20,true);
            this.carte[pos_x][pos_y].add(p);
            this.tabPersonnes[indiceDebut] = p;
        }
        indiceFin += nbExposees;
        //répartition personnes exposées
        for (indiceDebut = indiceDebut; indiceDebut < indiceFin; indiceDebut++) {
            pos_x = rand.nextInt(nbMax);
            pos_y = rand.nextInt(nbMax);
            Personne p = new Personne(indiceDebut,Categorie.EXPOSEE,pos_x,pos_y,20,true);
            this.carte[pos_x][pos_y].add(p);
            this.tabPersonnes[indiceDebut] = p;
        }
        indiceFin += nbInfectees;
        //répartition personnes infectées
        for (indiceDebut = indiceDebut; indiceDebut < indiceFin; indiceDebut++) {
            pos_x = rand.nextInt(nbMax);
            pos_y = rand.nextInt(nbMax);
            Personne p = new Personne(indiceDebut,Categorie.INFECTEE,pos_x,pos_y,20,true);
            this.carte[pos_x][pos_y].add(p);
            this.tabPersonnes[indiceDebut] = p;
        }
        indiceFin += nbRetirees;
        //répartition personnes retirées
        for (indiceDebut = indiceDebut; indiceDebut < indiceFin; indiceDebut++) {
            pos_x = rand.nextInt(nbMax);
            pos_y = rand.nextInt(nbMax);
            Personne p = new Personne(indiceDebut,Categorie.RETIREE,pos_x,pos_y,20,true);
            this.carte[pos_x][pos_y].add(p);
            this.tabPersonnes[indiceDebut] = p;
        }
    }
    
    


    /**
     * Fait déplacer les Personnes présentes sur la carte de manière aléatoire,
     * en prenant en compte si elles le peuvent ou non.
     */
    public void deplacement() {
        Random rand = new Random();
        int nbMax = 5;
        int direction;// 0 : nord, 1 : est, 2 : sud, 3 : ouest, 4 : aucun
        int pos1, pos2, newPos1, newPos2;
        boolean depl = false;
        for (int i = 0; i < this.nbPersonnes; i++) {
            if(this.tabPersonnes[i].getDeplacement()) {
                do {
                direction = rand.nextInt(nbMax);
                switch (direction) {
                case 0: //nord
                    if(this.tabPersonnes[i].getPosition(1) - 1 < 0) {
                        depl = false;
                        break;
                    } else {
                        depl = true;
                        break;
                    }
                case 1: //est
                    if(this.tabPersonnes[i].getPosition(0) + 1 > this.tailleCarte - 1) {
                        depl = false;
                        break;
                    } else {
                        depl = true;
                        break;
                    }
                case 2: //sud
                    if(this.tabPersonnes[i].getPosition(1) + 1 > this.tailleCarte - 1) {
                        depl = false;
                        break;
                    } else {
                        depl = true;
                        break;
                    }
                case 3: //ouest
                    if(this.tabPersonnes[i].getPosition(0) - 1 < 0) {
                        depl = false;
                        break;
                    } else {
                        depl = true;
                        break;
                    }
                case 4: //aucun
                    depl = true;
                    break;
                default:
                    depl = false;
                }
                
            } while(depl != true );
            
            pos1 = this.tabPersonnes[i].getPosition(0);
            pos2 = this.tabPersonnes[i].getPosition(1);
            
            switch (direction) {
                case 0:
                    this.carte[pos1][pos2].remove(this.tabPersonnes[i]);
                    newPos1 = pos1;
                    newPos2 = pos2 - 1;
                    this.tabPersonnes[i].setPosition(newPos1, newPos2);
                    this.carte[newPos1][newPos2].add(this.tabPersonnes[i]);
                    break;
                case 1:
                    this.carte[pos1][pos2].remove(this.tabPersonnes[i]);
                    newPos1 = pos1 + 1;
                    newPos2 = pos2;
                    this.tabPersonnes[i].setPosition(newPos1, newPos2);
                    this.carte[newPos1][newPos2].add(this.tabPersonnes[i]);
                    break;
                case 2:
                    this.carte[pos1][pos2].remove(this.tabPersonnes[i]);
                    newPos1 = pos1;
                    newPos2 = pos2 + 1;
                    this.tabPersonnes[i].setPosition(newPos1, newPos2);
                    this.carte[newPos1][newPos2].add(this.tabPersonnes[i]);
                    break;
                case 3:
                    this.carte[pos1][pos2].remove(this.tabPersonnes[i]);
                    newPos1 = pos1 - 1;
                    newPos2 = pos2;
                    this.tabPersonnes[i].setPosition(newPos1, newPos2);
                    this.carte[newPos1][newPos2].add(this.tabPersonnes[i]);
                    break;
                case 4:
                    break;
                default:
                    this.tabPersonnes[i].setPosition(pos1, pos2);
                    break;
                }
            }
            
        }
    }

    /**
     * Met à jour la carte selon le tableau de Personnes.
     */
    public void updateCarte() {
        int posX,posY;
        int tailleArray;
        //int j = 0;
        for(int i = 0; i < this.nbPersonnes; i++) {
            Personne p = new Personne(this.tabPersonnes[i]);
            posX = p.getPosition(0);
            posY = p.getPosition(1);
            tailleArray = this.carte[posX][posY].size();
            for(int j = 0; j<tailleArray; j++){
                if(this.carte[posX][posY].get(j).getIdentifiant() != p.getIdentifiant()) {
                    this.carte[posX][posY].set(j,this.tabPersonnes[i]);
                }
            }
        }
    }

    /**
     * Affiche dans la console la carte avec le nombre de Personnes par case.
     */
    public void afficher() {
        for (int i = 0; i < this.tailleCarte; i++) {
            for (int j = 0; j < this.tailleCarte; j++) {
                System.out.print("| " + this.carte[j][i].size() + " ");
            }
            System.out.println("");
        }
    }
    
    /**
     *
     * @param categorie Catégorie à vérifier
     * @param x Coordonnée x dans la carte
     * @param y Coordonnée y dans la carte
     * @return un booléen selon si une Personne est présente ou non avec la catégorie souhaitée
     */
    public Boolean checkCategorie(Categorie categorie, int x, int y){
        if(this.carte[x][y].size() <= 1){
            return false;
        }
        else {
            for(int i = 0; i<this.carte[x][y].size(); i++){
                if(this.carte[x][y].get(i).getCategorie().equals(categorie)){
                    return true;   
                }
            }
        }
        return false;
    }
    
    /**
     *
     * @param x Coordonnée de la case en x
     * @param y Coordonnée de la case en y
     * @return Le nombre de Personnes présentes dans la case aux coordonnée (x,y)
     */
    public int getSizeCarte(int x, int y){
        return this.carte[x][y].size();
    }
    
    /**
     *
     * @param categorie Catégorie à compter
     * @return Le nombre de Personnes sur la carte de la catégorie souhaitée
     */
    public int countCategorie(Categorie categorie){
        int count = 0;
        for(int i = 0; i< this.nbPersonnes; i++){
            if(this.tabPersonnes[i].getCategorie() == categorie){
                count++;
            }
        }
        return count;
    }
    
    /**
     *
     * @param categorie Catégorie à compter
     * @param x Coordonnée x de la carte
     * @param y Coordonnée y de la carte
     * @return Le nombre de Personnes dans la case (x,y) de la catégorie souhaitée
     */
    public int countCategorieXY(Categorie categorie, int x, int y){
        int count = 0;
        for(int i = 0; i< this.carte[x][y].size(); i++){
            if(this.carte[x][y].get(i).getCategorie() == categorie){
                count++;
            }
        }
        return count;
    }
    
    /**
     * Simule selon le modèle SIR avec les paramètres de la carte
     * 
     * @param t Durée de la simulation (en jour)
     * @param beta Probabilité qu'une personne infectée contamine une personne saine
     * @param gamma Probabilité de ne plus être infecté
     */
    public void simulerModeleSIR(int t, double beta, double gamma){
        SIR modeleSIR;
        int nbS = 0;
        int nbI = 0;
        int nbR = 0;
        int diffS, diffI, diffR;

        this.S = new int[t + 1];
        this.E = new int[t + 1];
        this.I = new int[t + 1];
        this.R = new int[t + 1];
        
        S[0] = this.nbSaines;
        I[0] = this.nbInfectees;
        R[0] = this.nbRetirees;
        
        for(int i = 1; i < t; i++){
            if(this.politiquePublique.getNom() != nomPolitiquePublique.PORT_DU_MASQUE) {
                        this.politiquePublique.appliquer();
                        clone(this.politiquePublique.getCarte());
                    }
            for(int x = 0; x < this.carte.length; x++){
                for(int y = 0; y < this.carte[x].length; y++){
                    
                    if(this.carte[x][y].size() > 0){
                        nbS = countCategorieXY(Categorie.SAINE, x, y);
                        nbI = countCategorieXY(Categorie.INFECTEE, x, y);
                        nbR = countCategorieXY(Categorie.RETIREE, x, y);
                        modeleSIR = new SIR(nbS, nbI, nbR, beta, gamma);
                        if(this.politiquePublique.getNom() == nomPolitiquePublique.PORT_DU_MASQUE) {
                            this.politiquePublique.setModele(modeleSIR);
                            this.politiquePublique.appliquer();
                            double newbeta = this.politiquePublique.getModele().getBeta();
                            clone(this.politiquePublique.getCarte());
                            modeleSIR.setBeta(newbeta);
                        }
                        
                        modeleSIR.executer(t);

                        diffS = countCategorieXY(Categorie.SAINE, x, y) - modeleSIR.getNbSaines();
                        updateNbSainesSIR(diffS, x, y);

                        diffI = countCategorieXY(Categorie.INFECTEE, x, y) - modeleSIR.getNbInfectees();
                        updateNbInfecteesSIR(diffI, x, y);

                        diffR = countCategorieXY(Categorie.RETIREE, x, y) - modeleSIR.getNbRetirees();
                        updateNbRetireesSIR(diffR, x, y); 
                    }
                    
                }
            } 
            
            this.nbSaines = countCategorie(Categorie.SAINE);
            this.nbExposees = countCategorie(Categorie.EXPOSEE);
            this.nbInfectees = countCategorie(Categorie.INFECTEE); 
            this.nbRetirees = countCategorie(Categorie.RETIREE);
            
            S[i] = this.nbSaines;
            E[i] = this.nbExposees;
            I[i] = this.nbInfectees;
            R[i] = this.nbRetirees;
            deplacement();
        }
    } 
    
    /**
     *
     * @param diffS Différence entre le nombre de Personnes saines donnée par
     * le modèle et la valeur actuelle présente sur la carte
     * @param x Coordonnées de la case en x
     * @param y Coordonnée de la case en y
     */
    public void updateNbSainesSIR(int diffS, int x, int y){
        //Random rand = new Random();
        //int indice_personne;
        Personne p = new Personne();        
        if(diffS > 0){ // this.nbSaines > modeleSIR.getNbSaines();
            while(diffS !=0){
                for(int i = 0; i<this.carte[x][y].size(); i++){
                    p.clone(this.carte[x][y].get(i));
                    if(p.getCategorie() == Categorie.SAINE){
                        p.setCategorie(Categorie.INFECTEE);
                        this.carte[x][y].get(i).clone(p);
                        diffS--;
                    }
                    if(diffS == 0){
                        i = this.carte[x][y].size();
                    }
                }
                diffS = 0; 
            }
        }else{ // this.nbSaines < modeleSIR.getNbSaines();
           while(diffS !=0){                              
                for(int i = 0; i<this.carte[x][y].size(); i++){
                    if(countCategorieXY(Categorie.RETIREE, x, y) == 0){
                        diffS = 0;
                    } 
                    p.clone(this.carte[x][y].get(i));
                    if(p.getCategorie() == Categorie.RETIREE){
                        p.setCategorie(Categorie.SAINE);
                        this.carte[x][y].get(i).clone(p);
                        diffS++;
                    }
                    if(diffS == 0){
                        i = this.carte[x][y].size();
                    }
                }
                diffS = 0; 
            }    
        } 
    }
            
    /**
     *
     * @param diffI Différence entre le nombre de Personnes infectées donnée par
     * le modèle et la valeur actuelle présente sur la carte
     * @param x Coordonnées de la case en x
     * @param y Coordonnée de la case en y
     */
    public void updateNbInfecteesSIR(int diffI, int x, int y){
        Personne p = new Personne();
        
        if(diffI > 0){ // this.nbInfectees > modeleSIR.getNbInfectees();
            while(diffI !=0){
                for(int i = 0; i<this.carte[x][y].size(); i++){
                    p.clone(this.carte[x][y].get(i));
                    if(p.getCategorie() == Categorie.INFECTEE){
                        p.setCategorie(Categorie.RETIREE);
                        this.carte[x][y].get(i).clone(p);
                        diffI--;
                    }
                    if(diffI == 0){
                        i = this.carte[x][y].size();
                    }
                }
                diffI = 0;
            }
        }else{ // this.nbInfectees < modeleSIR.getNbInfectees();
           while(diffI !=0){
                for(int i = 0; i<this.carte[x][y].size(); i++){
                    if(countCategorieXY(Categorie.SAINE, x, y) == 0){
                        diffI = 0;
                    } 
                    p.clone(this.carte[x][y].get(i));
                    if(p.getCategorie() == Categorie.SAINE){
                        p.setCategorie(Categorie.INFECTEE);
                        this.carte[x][y].get(i).clone(p);
                        diffI++;
                    }
                    if(diffI == 0){
                        i = this.carte[x][y].size();
                    }
                }
                diffI = 0;
            }    
        } 
    }
    
    /**
     *
     * @param diffR Différence entre le nombre de Personnes retirées donnée par
     * le modèle et la valeur actuelle présente sur la carte
     * @param x Coordonnées de la case en x
     * @param y Coordonnée de la case en y
     */
    public void updateNbRetireesSIR(int diffR, int x, int y){
        Personne p = new Personne();
        if(diffR > 0){ // this.nbRetirees > modeleSIR.getNbRetirees();
            while(diffR !=0){
                for(int i = 0; i<this.carte[x][y].size(); i++){
                    p.clone(this.carte[x][y].get(i));
                    if(p.getCategorie() == Categorie.RETIREE){
                        p.setCategorie(Categorie.SAINE);
                        this.carte[x][y].get(i).clone(p);
                        diffR--;
                    }
                    if(diffR == 0){
                        i = this.carte[x][y].size();
                    }
                }
                diffR = 0;
            }
        }else{ // this.nbRetirees < modeleSIR.getNbRetirees();
           while(diffR !=0){
                for(int i = 0; i<this.carte[x][y].size(); i++){
                    if(countCategorieXY(Categorie.INFECTEE, x, y) == 0){
                        diffR = 0;
                    }
                    p.clone(this.carte[x][y].get(i));
                    if(p.getCategorie() == Categorie.INFECTEE){
                        p.setCategorie(Categorie.RETIREE);
                        this.carte[x][y].get(i).clone(p);
                        diffR++;
                    }
                    if(diffR == 0){
                        i = this.carte[x][y].size();
                    }
                }
                diffR = 0;
            }    
        }
    }
    
    /**
     *
     * Simule selon le modèle SEIR avec les paramètres de la carte
     * 
     * @param t Durée de la simulation (en jour)
     * @param beta Probabilité qu'une personne infectée expose une personne saine
     * @param gamma Probabilité de ne plus être infecté
     * @param alpha Probabilté qu'une personne exposée devienne infectée
     */
    public void simulerModeleSEIR(int t, double beta, double gamma, double alpha){
        SEIR modeleSEIR;
        int nbS, nbE, nbI, nbR = 0;
        int diffS, diffE, diffI, diffR;
        
        this.S = new int[t+1];
        this.E = new int[t+1];
        this.I = new int[t+1];
        this.R = new int[t+1];
        
        this.S[0] = this.nbSaines;
        this.E[0] = this.nbExposees;
        this.I[0] = this.nbInfectees;
        this.R[0] = this.nbRetirees;
        
        for(int i = 1; i < t; i++){
            
            if(this.politiquePublique.getNom() != nomPolitiquePublique.PORT_DU_MASQUE) {
                this.politiquePublique.appliquer();
                clone(this.politiquePublique.getCarte());
            }
                        
            for(int x = 0; x < this.carte.length; x++){
                for(int y = 0; y < this.carte[x].length; y++){
                    
                    if(this.carte[x][y].size() > 1){
                        nbS = countCategorieXY(Categorie.SAINE, x, y);
                        nbE = countCategorieXY(Categorie.EXPOSEE, x, y);
                        nbI = countCategorieXY(Categorie.INFECTEE, x, y);
                        nbR = countCategorieXY(Categorie.RETIREE, x, y);
                        modeleSEIR = new SEIR(nbS, nbE, nbI, nbR, beta, gamma, alpha);
                        if(this.politiquePublique.getNom() == nomPolitiquePublique.PORT_DU_MASQUE) {
                            this.politiquePublique.setModele(modeleSEIR);
                            this.politiquePublique.appliquer();
                            double newbeta = this.politiquePublique.getModele().getBeta();
                            clone(this.politiquePublique.getCarte());
                            modeleSEIR.setBeta(newbeta);
                        }
                        
                        modeleSEIR.executer(t);

                        diffS = countCategorieXY(Categorie.SAINE, x, y) - modeleSEIR.getNbSaines();
                        updateNbSainesSEIR(diffS, x, y);

                        diffE = countCategorieXY(Categorie.EXPOSEE, x, y) - modeleSEIR.getNbExposees();
                        updateNbExposeesSEIR(diffE, x, y);
            
                        diffI = countCategorieXY(Categorie.INFECTEE, x, y) - modeleSEIR.getNbInfectees();
                        updateNbInfecteesSEIR(diffI, x, y);

                        diffR = countCategorieXY(Categorie.RETIREE, x, y) - modeleSEIR.getNbRetirees();
                        updateNbRetireesSEIR(diffR, x, y);                    
                    }   
                }
            }
            
            this.nbSaines = countCategorie(Categorie.SAINE);
            this.nbExposees = countCategorie(Categorie.EXPOSEE);
            this.nbInfectees = countCategorie(Categorie.INFECTEE); 
            this.nbRetirees = countCategorie(Categorie.RETIREE);
             
            S[i] = this.nbSaines;
            E[i] = this.nbExposees;
            I[i] = this.nbInfectees;
            R[i] = this.nbRetirees;
            deplacement();
        }
    }
    
    /**
     *
     * @param diffS Différence entre le nombre de Personnes saines donnée par
     * le modèle et la valeur actuelle présente sur la carte
     * @param x Coordonnées de la case en x
     * @param y Coordonnée de la case en y
     */
    public void updateNbSainesSEIR(int diffS, int x, int y){
        Personne p = new Personne();        
        if(diffS > 0){ // this.nbSaines > modeleSEIR.getNbSaines();
            while(diffS !=0){
                for(int i = 0; i<this.carte[x][y].size(); i++){
                    p.clone(this.carte[x][y].get(i));
                    if(p.getCategorie() == Categorie.SAINE){
                        p.setCategorie(Categorie.EXPOSEE);
                        this.carte[x][y].get(i).clone(p);
                        diffS--;
                    }
                    if(diffS == 0){
                        i = this.carte[x][y].size();
                    }
                }
                diffS = 0;
            }
        }else{ // this.nbSaines < modeleSEIR.getNbSaines();
           while(diffS !=0){
                for(int i = 0; i<this.carte[x][y].size(); i++){
                    if(countCategorieXY(Categorie.RETIREE, x, y) == 0){
                        diffS = 0;
                    } 
                    p.clone(this.carte[x][y].get(i));
                    if(p.getCategorie() == Categorie.RETIREE){
                        p.setCategorie(Categorie.SAINE);
                        this.carte[x][y].get(i).clone(p);
                        diffS++;
                    }
                    if(diffS == 0){
                        i = this.carte[x][y].size();
                    }
                }
                diffS = 0;
            }    
        }
    }
    
    /**
     *
     * @param diffE Différence entre le nombre de Personnes exposées donnée par
     * le modèle et la valeur actuelle présente sur la carte
     * @param x Coordonnées de la case en x
     * @param y Coordonnée de la case en y
     */
    public void updateNbExposeesSEIR(int diffE, int x, int y){
        Personne p = new Personne();        
        if(diffE > 0){ // this.nbExposees > modeleSEIR.getNbExposees();
            while(diffE !=0){
                for(int i = 0; i<this.carte[x][y].size(); i++){
                    p.clone(this.carte[x][y].get(i));
                    if(p.getCategorie() == Categorie.EXPOSEE){
                        p.setCategorie(Categorie.INFECTEE);
                        this.carte[x][y].get(i).clone(p);
                        diffE--;
                    }
                    if(diffE == 0){
                        i = this.carte[x][y].size();
                    }
                }
                diffE = 0;
            }
        }else{ // this.nbExposees < modeleSEIR.getNbExposees();
           while(diffE !=0){
                for(int i = 0; i<this.carte[x][y].size(); i++){
                    if(countCategorieXY(Categorie.SAINE, x, y) == 0){
                        diffE = 0;
                    } 
                    p.clone(this.carte[x][y].get(i));
                    if(p.getCategorie() == Categorie.SAINE){
                        p.setCategorie(Categorie.EXPOSEE);
                        this.carte[x][y].get(i).clone(p);
                        diffE++;
                    }
                    if(diffE == 0){
                        i = this.carte[x][y].size();
                    }
                }
                diffE = 0;
            }    
        }
    }
    
    /**
     *
     * @param diffI Différence entre le nombre de Personnes infectées donnée par
     * le modèle et la valeur actuelle présente sur la carte
     * @param x Coordonnées de la case en x
     * @param y Coordonnée de la case en y
     */
    public void updateNbInfecteesSEIR(int diffI, int x, int y){
        Personne p = new Personne();
        if(diffI > 0){ // this.nbInfectees > modeleSEIR.getNbInfectees();
            while(diffI !=0){
                for(int i = 0; i<this.carte[x][y].size(); i++){
                    p.clone(this.carte[x][y].get(i));
                    if(p.getCategorie() == Categorie.INFECTEE){
                        p.setCategorie(Categorie.RETIREE);
                        this.carte[x][y].get(i).clone(p);
                        diffI--;
                    }
                    if(diffI == 0){
                        i = this.carte[x][y].size();
                    }                      
                }
                diffI = 0;
            }
        }else{ // this.nbInfectees < modeleSEIR.getNbInfectees();
           while(diffI !=0){
                for(int i = 0; i<this.carte[x][y].size(); i++){
                    if(countCategorieXY(Categorie.EXPOSEE, x, y) == 0){
                        diffI = 0;
                    } 
                    p.clone(this.carte[x][y].get(i));
                    if(p.getCategorie() == Categorie.EXPOSEE){
                        p.setCategorie(Categorie.INFECTEE);
                        this.carte[x][y].get(i).clone(p);
                        diffI++;
                    }
                    if(diffI == 0){
                        i = this.carte[x][y].size();
                    }                    
                }
                diffI = 0;
            }    
        }
    }
    
    /**
     *
     * @param diffR Différence entre le nombre de Personnes retirées donnée par
     * le modèle et la valeur actuelle présente sur la carte
     * @param x Coordonnées de la case en x
     * @param y Coordonnée de la case en y
     */
    public void updateNbRetireesSEIR(int diffR, int x, int y){
        Personne p = new Personne();
        if(diffR > 0){ // this.nbRetirees > modeleSEIR.getNbRetirees();
            while(diffR !=0){
                for(int i = 0; i<this.carte[x][y].size(); i++){
                    p.clone(this.carte[x][y].get(i));
                    if(p.getCategorie() == Categorie.RETIREE){
                        p.setCategorie(Categorie.SAINE);
                        this.carte[x][y].get(i).clone(p);
                        diffR--;
                    }
                    if(diffR == 0){
                        i = this.carte[x][y].size();
                    }                     
                }
                diffR = 0;
            }
        }else{ // this.nbRetirees < modeleSEIR.getNbRetirees();
           while(diffR !=0){
                for(int i = 0; i<this.carte[x][y].size(); i++){
                    if(countCategorieXY(Categorie.INFECTEE, x, y) == 0){
                        diffR = 0;
                    } 
                    p.clone(this.carte[x][y].get(i));
                    if(p.getCategorie() == Categorie.INFECTEE){
                        p.setCategorie(Categorie.RETIREE);
                        this.carte[x][y].get(i).clone(p);
                        diffR++;
                    }
                    if(diffR == 0){
                        i = this.carte[x][y].size();
                    } 
                }
                diffR = 0;
            }    
        }
    }
    
    /**
     *
     * Simule selon le modèle SEIR avec naissances et morts naturelles 
     * avec les paramètres de la carte
     * 
     * @param t Durée de la simulation (en jour)
     * @param beta Probabilité qu'une personne infectée expose une personne saine
     * @param gamma Probabilité de ne plus être infecté
     * @param alpha Probabilté qu'une personne exposée devienne infectée
     * @param mu Proportion de Personnes qui meurent naturellement
     * @param eta Proportion de Personnes qui naissent
     */
    public void simulerModeleSEIRnaiss(int t, double beta, double gamma, double alpha, double mu, double eta){
        SEIRnaiss modeleSEIRnaiss;
        int nbS, nbE, nbI, nbR = 0;
        int diffS, diffE, diffI, diffR;
        
        this.S = new int[t+1];
        this.E = new int[t+1];
        this.I = new int[t+1];
        this.R = new int[t+1];
        
        this.S[0] = this.nbSaines;
        this.E[0] = this.nbExposees;
        this.I[0] = this.nbInfectees;
        this.R[0] = this.nbRetirees;
        
        for(int i = 1; i < t; i++){
            if(this.politiquePublique.getNom() != nomPolitiquePublique.PORT_DU_MASQUE) {
                this.politiquePublique.appliquer();
                clone(this.politiquePublique.getCarte());
            }
            for(int x = 0; x < this.carte.length; x++){
                for(int y = 0; y < this.carte[x].length; y++){                    
                    if(this.carte[x][y].size() > 1){
                        nbS = countCategorieXY(Categorie.SAINE, x, y);
                        nbE = countCategorieXY(Categorie.EXPOSEE, x, y);
                        nbI = countCategorieXY(Categorie.INFECTEE, x, y);
                        nbR = countCategorieXY(Categorie.RETIREE, x, y);
                        modeleSEIRnaiss = new SEIRnaiss(nbS, nbE, nbI, nbR, beta, gamma, alpha, mu, eta);
                        if(this.politiquePublique.getNom() == nomPolitiquePublique.PORT_DU_MASQUE) {
                            this.politiquePublique.setModele(modeleSEIRnaiss);
                            this.politiquePublique.appliquer();
                            double newbeta = this.politiquePublique.getModele().getBeta();
                            clone(this.politiquePublique.getCarte());
                            modeleSEIRnaiss.setBeta(newbeta);
                        }
                        modeleSEIRnaiss.executer(t);
                        
                        diffS = countCategorieXY(Categorie.SAINE, x, y) - modeleSEIRnaiss.getNbSaines();
                        updateNbSainesSEIRnaiss(diffS, x, y);

                        diffE = countCategorieXY(Categorie.EXPOSEE, x, y) - modeleSEIRnaiss.getNbExposees();
                        updateNbExposeesSEIR(diffE, x, y);
            
                        diffI = countCategorieXY(Categorie.INFECTEE, x, y) - modeleSEIRnaiss.getNbInfectees();
                        updateNbInfecteesSEIR(diffI, x, y);

                        diffR = countCategorieXY(Categorie.RETIREE, x, y) - modeleSEIRnaiss.getNbRetirees();
                        updateNbRetireesSEIRnaiss(diffR, x, y);
                    }   
                }
            }
            
            this.nbSaines = countCategorie(Categorie.SAINE);
            this.nbExposees = countCategorie(Categorie.EXPOSEE);
            this.nbInfectees = countCategorie(Categorie.INFECTEE); 
            this.nbRetirees = countCategorie(Categorie.RETIREE);
            
            S[i] = this.nbSaines;
            E[i] = this.nbExposees;
            I[i] = this.nbInfectees;
            R[i] = this.nbRetirees;
            deplacement();
        }        
    }
    
    /**
     *
     * @param diffS Différence entre le nombre de Personnes saines donnée par
     * le modèle et la valeur actuelle présente sur la carte
     * @param x Coordonnées de la case en x
     * @param y Coordonnée de la case en y
     */
    public void updateNbSainesSEIRnaiss(int diffS, int x, int y){
        Random rand = new Random();
        Personne p = new Personne();        
        if(diffS > 0){ // this.nbSaines > modeleSEIR.getNbSaines();
            while(diffS !=0){
                for(int i = 0; i<this.carte[x][y].size(); i++){
                    p.clone(this.carte[x][y].get(i));
                    if(p.getCategorie() == Categorie.SAINE){
                        p.setCategorie(Categorie.EXPOSEE);
                        this.carte[x][y].get(i).clone(p);
                        diffS--;
                    }
                    if(diffS == 0){
                        i = this.carte[x][y].size();
                    }
                }
                diffS = 0;
            }
        }else{ // this.nbSaines < modeleSEIR.getNbSaines();
           while(diffS !=0){
                int n = abs(diffS);
                for(int i = 0; i<n; i++){
                    int id = this.nbPersonnes;
                    Personne newPersonne = new Personne(id, Categorie.SAINE, x, y, 20, true);
                    p.clone(newPersonne);
                    Personne[] newTabPersonnes = new Personne[this.nbPersonnes + 1];
                    for(int j = 0; j< this.tabPersonnes.length; j++){
                        newTabPersonnes[j] = this.tabPersonnes[j];
                    }
                    newTabPersonnes[this.tabPersonnes.length] = p;
                    this.tabPersonnes = new Personne[newTabPersonnes.length];
                    this.tabPersonnes = newTabPersonnes; 
                    this.nbPersonnes++;
                    this.carte[x][y].add(p);
                    diffS++;
                    
                    if(diffS == 0){
                        i = n;
                    }
                }
                diffS = 0;
            }    
        }
    }
    
    /**
     *
     * @param diffR Différence entre le nombre de Personnes retirées donnée par
     * le modèle et la valeur actuelle présente sur la carte
     * @param x Coordonnées de la case en x
     * @param y Coordonnée de la case en y
     */
    public void updateNbRetireesSEIRnaiss(int diffR, int x, int y){
        Personne p = new Personne();
        if(diffR > 0){ // this.nbRetirees > modeleSEIR.getNbRetirees();
            while(diffR !=0){
                for(int i = 0; i<this.carte[x][y].size(); i++){
                    p.clone(this.carte[x][y].get(i));
                    if(p.getCategorie() == Categorie.RETIREE){
                        this.carte[x][y].remove(i);
                        diffR--;
                    }
                    if(diffR == 0){
                        i = this.carte[x][y].size();
                    }
                }
                diffR = 0;
            }
        }else{ // this.nbRetirees < modeleSEIR.getNbRetirees();
           while(diffR !=0){
                for(int i = 0; i<this.carte[x][y].size(); i++){
                    if(countCategorieXY(Categorie.INFECTEE, x, y) == 0){
                        diffR = 0;
                    }                    
                    p.clone(this.carte[x][y].get(i));
                    if(p.getCategorie() == Categorie.INFECTEE){
                        p.setCategorie(Categorie.RETIREE);
                        this.carte[x][y].get(i).clone(p);
                        diffR++;
                    }
                    if(diffR == 0){
                        i = this.carte[x][y].size();
                    }
                }
                diffR = 0;
            }    
        }

    }
}
