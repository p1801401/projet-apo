/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simulationepidemie.models;

/**
 *
 * @author somir
 */


public class Personne {
    
    /**
     * Toutes les catégories possibles
     */
    public enum Categorie {

        /**
         * Catégorie "saine"
         */
        SAINE, 

        /**
         * Catégorie "exposée"
         */
        EXPOSEE, 

        /**
         * Catégorie "inféctée"
         */
        INFECTEE, 

        /**
         * Catégorie "retirée"
         */
        RETIREE};
    
    /**
    * Identifiant unique à chaque Personne
    */
    private int identifiant;
    
    /**
    * Catégorie de la Personne
    */
    private Categorie categorie;
    
    /**
    * La position (x,y) de la Personne sur la carte
    */
    private int pos_x, pos_y;
    
    /**
    * Age de la Personne
    */
    private int age;
    
    /**
    * Possibilité de se déplacer de la Personne
    */
    private Boolean deplacement; 
    
    /**
     *
     * @param identifiant Identifiant unique
     * @param cat Catégorie de la Personne
     * @param pos1 Position x de la Personne sur la carte
     * @param pos2 Position y de la Personne sur la carte
     * @param age Age de la Personne
     * @param deplacement Si la Personne peut se déplacer ou non
     */
    public Personne(int identifiant, Categorie cat, int pos1, int pos2, int age, Boolean deplacement){
        this.identifiant = identifiant;
        this.categorie = cat;
        this.pos_x = pos1; 
        this.pos_y = pos2;
        this.age = age;
        this.deplacement = deplacement;
    }
    
    /**
     * Constructeur par défaut
     */
    public Personne(){
        this(0, null, 0, 0, 0, true);
    }
    
    /**
     * Constructeur par copie
     * @param p Personne à copier
     */
    public Personne(Personne p) {
        this.identifiant = p.identifiant;
        this.categorie = p.categorie;
        this.pos_x = p.pos_x;
        this.pos_y = p.pos_y;
        this.age = p.age;
        this.deplacement = p.deplacement;
    }
    
    /**
     *
     * @return L'identifiant de la Personne
     */
    public int getIdentifiant(){
        return identifiant;
    }
    
    /**
     *
     * @return La catégorie de la Personne
     */
    public Categorie getCategorie(){
        return categorie;
    }
    
    /**
     *
     * @param indice 0 : position x ; sinon : position y
     * @return La valeur d'une de ses coordonnées (x ou y) sur la carte
     */
    public int getPosition(int indice){
        if (indice == 0) {
            return pos_x;
        }
        return pos_y;
    }
    
    /**
     *
     * @return L'age de la Personne
     */
    public int getAge(){
        return age;
    }
    
    /**
     *
     * @return Si oui ou non la Personne eptu se déplacer
     */
    public Boolean getDeplacement(){
        return this.deplacement;
    }
    
    /**
     *
     * @param categorie Catégorie que remplace celle actuelle
     */
    public void setCategorie(Categorie categorie){
        this.categorie = categorie;
    }
    
    /**
     *
     * @param deplacement Valeur de déplacement qui remplace celle actuelle
     */
    public void setDeplacement(Boolean deplacement){
        this.deplacement = deplacement;
    }
    
    /**
     *
     * @param val1 Position x sur la carte que l'on veut remplacer
     * @param val2 Position y sur la carte que l'on veut remplacer
     */
    public void setPosition(int val1, int val2) {
        this.pos_x = val1;
        this.pos_y = val2;
    }
    
    /**
     *
     * @param p Personne à cloner
     */
    public void clone(Personne p){
        this.identifiant = p.getIdentifiant();
        this.categorie = p.getCategorie();
        this.pos_x = p.getPosition(0); 
        this.pos_y = p.getPosition(1);
        this.age = p.getAge();
        this.deplacement = p.getDeplacement();
    }
}
