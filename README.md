# Projet SimulationEpidemie
------------------------------------------------------
### Sommaire
* Comment lancer le programme ?
* Les fonctionnalités
* Articulation du projet

------------------------------------------------------
#### Comment lancer le programme ?
Il y a deux manières de lancer le programme :
1. A partir d'un IDE (comme NetBeans)
2. A l'aide d'un terminal

##### 1. A partir d'un IDE (comme NetBeans)
Il suffit de **Run** le projet à partir de l'IDE 

##### 2. A l'aide d'un terminal 
Nous avons mis en place des commandes à l'aide de l'outil Ant. Pour pouvoir bénificier de ces commandes, il faut :
+ [Installer Ant]( https://ant.apache.org/) sur votre ordinateur. 
+ Il faut également ouvrir un terminal et aller à l'emplacement où se trouve le projet.

Une fois ces étapes éffectué, vous pouvez utiliser les commandes *ant*
Les commandes principales sont :
+ *ant compile* => Permet de compiler le projet et de mettre les fichiers compiler dans le dossier *bin*
+ *ant jar* => Permet de créer un executable SimulationEpidemie.jar dans le dossier *bin*
+ *ant javadoc* => Permet de générer la javadoc dans le dossier *doc*
+ *ant run* => Permet de lancer le programme

> **En pratique, seule la commande *ant run* vous sera nécessaire car nous avons déjà fournit le dossier bin et doc**  

------------------------------------------------------
#### Les fonctionnalités
 + Lorsqu'on lance le programme, on se retrouve sur un menu. A partir de ce menu, l'utilisateur peut décider quel type de simulation il veut (SIR, SEIR ou SEIRnaiss). 
Une fois choisi, des zones de textes apparaissent pour que l'utilisateur puisse entrer les paramètres de sa simulation. Nous avons décidé de mettre des valeurs par défaut dans chaque zone de texte pour qu'un utilisateur non initité puisse lancer et aprprécier une simulation.
Nous avons également prévu le cas où l'utilisateur décide de changer modèle/type de simulation. Dans ce cas il devras appuyer sur le bouton *Retour* en haut à droite de la fenêtre; ce qui le redirigera au menu principal.

 + Une fois la simulation lancé, une nouvelle fenêtre s'ouvre. Dans celle-ci l'utilisateur à le choix de la mise en forme des résultats. Il a le choix d'observer les résultats sous la forme d'un camembère, d'un tableau ou d'un graphe. Tout comme le menu, l'utilisateur peut basculer d'une mise en forme à une autre par le biais du bouton *Retour*. 

 - *Faites attentions aux paramètres de départ que vous entrez car si vous entrez des valeurs trop grandes, cela risque de mettre du temps avant de générer la fenêtre des résultats*  

------------------------------------------------------
#### L'articulation du projet 
Nous avons décidé de construire notre projet sous un modèle MVC. Ainsi nous avons 3 dossiers :
+ Models
+ Views
+ Controllers

Avec ce modèle, il est facile d'ajouter des fonctionnalités.
De plus, l'utilisateur n'a accès qu'aux views, ce qui est apreciable étant donné qu'il n'a besoin d'aucune connaissance en codage pour lancer une simulation.      

A chaque fois qu'une simlation est lancé, une instance d'un *Controller* adapté est crée. Ce dernier crée à son tour une *Carte* avec les paramètres entrés par l'utilisateur et lance une simulation dans la carte.
Une fois la simulation effectué, on récupère les tableaux dans la carte qui contiennent les resultats de la simation et on crée une nouvelle *View* pour afficher ces résultats.
Le fait qu'une nouvelle fenêtre est généré au lancement de chaque simulation, il est possible de lancer une simulation, puis une autre et de comparer leur résultats.  
